import {
  ACTION_KEY_POST_LOGIN,
  ACTION_KEY_POST_LOGIN_ERROR,
  ACTION_KEY_POST_REGISTER,
  ACTION_KEY_POST_REGISTER_ERROR,
  SIGN_OUT_USER,
  ACTION_KEY_SET_CURRENT_USER,
  getRequestStatus
} from "services/constants";
import isEmpty from "validations/is-empty";
import { getActionTypes } from "services/api";

const InitialState = {
  authenticated: false,
  errorMessage: {},
  currentUser: {},
  fetching:false
};
export const userAuthReducer = (state = InitialState, action) => {
  const reducerActionType = getActionTypes(ACTION_KEY_POST_LOGIN)
  switch (action.type) {
    case reducerActionType.FETCHING:
      return {
        ...state,
        authenticated: false,
        currentUser: {},
        fetching:true,
        errorMessage:{}
      };
      case reducerActionType.FULFILLED:
        return{
          ...state,
          authenticated:true,
          currentUser:action.payload,
          fetching:false,
          errorMessage:{}
        };
        case reducerActionType.REJECTED:
          return{
            ...state,
            authenticated:false,
            currentUser:{},
            fetching:false,
            errorMessage:action.payload
          }
        
    case SIGN_OUT_USER:
      return {
        ...state,
        authenticated: false,
        currentUser: {},
        fetching:false,
        errorMessage:{}
      };
    // case ACTION_KEY_POST_LOGIN_ERROR:
    //   return {
    //     ...state,
    //     authenticated: false,
    //     errorMessage: action.payload,
    //     currentUser: {}
    //   };
    default:
      return state;
  }
};

export const userRegistrationReducer = (
  state = { status: "", userAction: "" },
  action
) => {
  switch (action.type) {
    case ACTION_KEY_POST_REGISTER:
      return {
        ...state,
        status: action.payload,
        userAction: action.userAction
      };
    case ACTION_KEY_POST_REGISTER_ERROR:
      return {
        ...state,
        errorMessage: action.payload
      };
    default:
      return state;
  }
};
