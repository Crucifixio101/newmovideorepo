import React from "react";
import {
  BASE_URL,
  IMAGE_POSTER_SIZES,
  COLLECTIONS_POSTER_SIZES
} from "services/api";
import {DUPLICATE_MOVIE_TITLE} from "services/constants";
import { getCurrentUserCollections } from "store/actions/Collections";
import {allMoviesTitleSelector} from "store/selectors/latestMovies"
import axios from "axios";
import { API_URL } from "services/api";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import store from "store/store";
import "./card.scss";

export class ImageCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLocation: "",
      titles: []
    };
  }
  componentDidMount() {
    axios.get(API_URL.GET_CURRENT_USER_COLLECTIONS)
      .then(response =>  this.props.getCurrentUserCollections(response.data))
      .catch(error => console.log(error.response.data))
    let currentLocation = this.props.history.location.pathname;
    this.setState({ currentLocation});
  }
  renderImageCardJSX(arrayToMap, btnText) {
    const jsx =
      arrayToMap &&
      arrayToMap.map((item, index) => {
        let posterPath =
          this.state.currentLocation === "/my-collections"
            ? `${BASE_URL}${IMAGE_POSTER_SIZES}${item.poster}`
            : `${BASE_URL}${IMAGE_POSTER_SIZES}${item.poster_path}`;
        const requestBody = {
          title: item.title,
          overview: item.overview,
          year: item.release_date,
          poster: item.poster_path,
          rating: item.vote_average,
          movieId: item.id,
          date: Date.now()
        };
        /**
         *  {options} object not added to the payload because the token as been configured to be added added to each request following the initial validation
         * check utils.js file for the setting of Auth token in setAuthToken()
         * this was achieved using axios  ----> axios.defaults.headers.common["Authorization"] = token;
         */
        // const options = {
        //   headers: { token: this.props.token }
        // };

        const saveToCollectionsHandler = () => {
          console.log(this.props.titles)
          // check if movie exist in collection
          // UI validation for duplicate movies
          const hasDuplicate = this.props.titles && this.props.titles.filter((item) => {
            return item === requestBody.title
          })
          console.log(hasDuplicate)
          if(hasDuplicate.length > 0){
               store.dispatch({type:DUPLICATE_MOVIE_TITLE, payload:"Movie already exist in collection"})
          }else{
            axios
            //update user collection in DB
            .post(API_URL.POST_ADD_MOVIE, requestBody)
              //if successful, get the current collection list
              axios.get(API_URL.GET_CURRENT_USER_COLLECTIONS)
                .then(response =>  this.props.getCurrentUserCollections(response.data))
                .catch(error => console.log(error.response.data))
            //save the selected movie to db
            //if successful, make a get cal to get all the movies for that particular user
            //save the get response to store
            //connect the collections component to store. when component mounts,fetch the list of movies from store
          }
        };

        return (
          <div key={`${item.id}${index}`} className="movie-card">
            <img
              src={posterPath}
              alt="movie poster"
              className="movie-card_poster"
            />
            {/**
             * @param saveToCollectionsHandler action to be called for ImageCard on other pages
             * this div will be rendered for other pages
             */}

            {
              <>
                {this.state.currentLocation !== "/my-collections" ? (
                  <div className="movie-card_other-items">
                    <h4 className="movie-card_other-items__title">
                      {item.title}
                    </h4>
                    <h4 className="movie-card_other-items__rating">
                      {item.vote_average}
                    </h4>
                    <a
                      href="#"
                      className="movie-card_other-items__save-btn"
                      onClick={() => {
                        saveToCollectionsHandler();
                      }}
                    >
                      Save To Collections
                    </a>
                  </div>
                ) : (
                    <div className="movie-card_other-items">
                      <h4 className="movie-card_other-items__title">
                        {item.title}
                      </h4>
                      <h4 className="movie-card_other-items__rating">
                        {item.vote_average}
                      </h4>
                      <a
                        href="#"
                        className="movie-card_other-items__save-btn"
                        onClick={() => {
                          saveToCollectionsHandler();
                        }}
                      >
                        Move To Watched
                    </a>
                    </div>
                  )}
              </>
            }
          </div>
        );
      });
    return jsx;
  }
  render() {
    const { list, propsObj } = this.props;
    const btnText = propsObj && propsObj.btnText;
    return (
      <div className="images-cards-container">
        {this.renderImageCardJSX(list || [], btnText || {})}
      </div>
    );
  }
}

export const mapStateToProps = state => {
  return {
    token: state.auth.authenticated,
    titles: allMoviesTitleSelector(state)
  };
};


export default compose(
  connect(mapStateToProps, { getCurrentUserCollections }),
  withRouter
)(ImageCard);
