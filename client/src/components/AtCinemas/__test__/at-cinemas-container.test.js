import React from "react";
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from "enzyme";
import { AtCinemasContainer, mapStateToProps } from "../AtCinemasContainer";
configure({ adapter: new Adapter() });


describe("Test AtCinemasContainer", () => {
    let state, props;
    props = {
        moviesAtCinemas: [
            {
                test: "test"
            }
        ],
        getMoviesAtCinemas: jest.fn()
    }

    const wrapper = shallow(<AtCinemasContainer {...props} />);
    it("renders correctly", () => {
        expect(wrapper).toHaveLength(1);
    })
})