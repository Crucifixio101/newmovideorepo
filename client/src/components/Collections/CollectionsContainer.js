import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux"
import { withRouter } from "react-router-dom";
import CollectionsPresentation from "../Collections/CollectionsPresentation";
import { getCurrentUserCollections } from "store/actions/Collections";
import { API_URL } from "services/api";
import axios from "axios";
import { ACTION_KEY_POST_LOGIN_ERROR } from "services/constants";
import store from "store/store";
import { signOutUser } from "auth/authAction";
class CollectionsContainer extends Component {
  getUserCollections = () => {
    axios
      .get(API_URL.GET_CURRENT_USER_COLLECTIONS)
      .then(res => this.props.getCurrentUserCollections(res.data))
      .catch(err => store.dispatch({ type: ACTION_KEY_POST_LOGIN_ERROR, payload: err.response.data }))
  };

  async componentDidMount() {
    //if user isn't authenticated or logged in redirect to login page
    if (!this.props.isLoggedIn) {
      await this.props.history.push("./");
    }
    await this.getUserCollections();
  }

  async componentDidUpdate() {
    //if user isn't authenticated or logged in redirect to login page
    if (!this.props.isLoggedIn) {
      await this.props.history.push("./");
    }
  }

  render() {
    const { moviesCollections } = this.props;

    return (
      <div>
        <CollectionsPresentation list={moviesCollections} />
      </div>
    );
  }
}

export const mapStateToProps = state => {
  return {
    moviesCollections: state.currentUserCollections.usersCollection,
    isLoggedIn: state.auth.authenticated

  };
};
// export default connect(mapStateToProps, { getCurrentUserCollections, signOutUser })(
//   CollectionsContainer
// );

export default compose(
  connect(mapStateToProps, { getCurrentUserCollections, signOutUser }),
  withRouter,
)(CollectionsContainer)