import {getDefaultString, getDefaultObj, getDefaultNumber} from "../services/helper";

export default class RecentMoviesModel{
    constructor(init={}){
        this.convertToModel(init)
    }

    convertToModel(init={}){
        this.popularity = getDefaultString(init.popularity);
        this.poster_path = getDefaultString(init.poster_path);
        this.id = getDefaultNumber(init.id);
        this.title = getDefaultString(init.title);
        this.vote_average = getDefaultNumber(init.vote_average);
        this.overview = getDefaultString(init.overview);
        this.release_date = getDefaultString(init.release_date)
    }
}