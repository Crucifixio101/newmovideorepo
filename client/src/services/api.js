import axios from "axios";

/****** Movie Urls*****************/
export const API_URL = {
  latestMovies:
    "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=2afb1d4ebd6b0b7548fc20dd30d0ac26",
  atCinemas:
    "https://api.themoviedb.org/3/movie/now_playing?page=1&language=en-US&api_key=2afb1d4ebd6b0b7548fc20dd30d0ac26",
  OSCAR_2012:
    "https://api.themoviedb.org/3/list/2?language=en-US&api_key=2afb1d4ebd6b0b7548fc20dd30d0ac26",
  OSCAR_2011:
    "https://api.themoviedb.org/3/list/4?language=en-US&api_key=2afb1d4ebd6b0b7548fc20dd30d0ac26",
  OSCAR_2010:
    "https://api.themoviedb.org/3/list/8?language=en-US&api_key=2afb1d4ebd6b0b7548fc20dd30d0ac26",
  OCSAR_2009:
    "https://api.themoviedb.org/3/list/9?language=en-US&api_key=2afb1d4ebd6b0b7548fc20dd30d0ac26",
  POST_LOGIN: "http://localhost:5000/api/users/login",
  POST_REGISTER: "http://localhost:5000/api/users/register",
  POST_ADD_MOVIE: "http://localhost:5000/api/collections/add",
  GET_CURRENT_USER_COLLECTIONS:
    "http://localhost:5000/api/collections/movies/logged-user",
  VALIDATE_CURRENT_USER_TOKEN: "http://localhost:5000/api/users/current"
};
//CONFIGURATION API FOR MOVIES ENDPOINT//
export const BASE_URL = "http://image.tmdb.org/t/p/";
// export const POSTER_SIZES = "w500";
export const POSTER_SIZES = "w342";
export const COLLECTIONS_POSTER_SIZES = "w154";
export const IMAGE_POSTER_SIZES = "w154";
// export const POSTER_SIZES = "w154";
// export const POSTER_SIZES = "w185";

//use switch statement to render different poster_sizes based onmedia-query ranges


/*****************************************
 * Action and Reducer Keys
 * ***************************************/

 export const getActionTypes = (actionKey) => {
  return {
    FETCHING:`${actionKey}_fetching`,
    FULFILLED:`${actionKey}_fulfilled`,
    REJECTED:`${actionKey}_rejected`
  }
 }

 export const HTTP = {
     GET:"get",
     POST:"post",
     PUT:"put"
 }

//  export const makeAxiosCall = (httpMethod, url, reqBody={}) => (dispatch = ({type="", payload = null}) => {}, actionKey = "") => {
//   console.log("httpMethod",httpMethod)
//   console.log("url",url)
//   console.log("reqBody",reqBody)
//   const ACTION_TYPES = getActionTypes(actionKey)
//   try {
//     if(url){
//       dispatch({type:ACTION_TYPES.FETCHING})
//       if(httpMethod === HTTP.GET){
//         axios.get(url)
//         .then(res => dispatch({type:ACTION_TYPES.FULFILLED, payload:res}))
//       }
//       if(httpMethod === HTTP.PUT){
//         axios.put(url, reqBody)
//         .then(response => dispatch({type:ACTION_TYPES.FULFILLED, payload:response})) 
//       }
//     }
//   }
//   catch(e){
//     console.log("error is", e)
//       dispatch({type:ACTION_TYPES.REJECTED, payload:e.message})
//   }
//  }