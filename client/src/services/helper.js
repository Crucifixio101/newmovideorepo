import axios from "axios";
import { API_URL } from "services/api";
/**
 * 
 * @param {string} str 
 * The function returns an empty string if str is undefined or null
 */
export const getDefaultString = (str) => {
    if(str === undefined || str === "undefined" || str === null || str === "null" || !str){
        return "";
    }
    return str
    }

    /**
     * 
     * @param {object} obj 
     * The function returns an empty object if obj is null or undefined
     */
export const getDefaultObj = (obj) => {
    if(typeof obj === "object" && obj === null || obj === undefined || !(Object.keys(obj).length > 0)){
        return {}
    }
    return obj
    } 
   /**
    * 
    * @param {number} num - The Number.isFinite() function checks if the variable is a number,
    * but also checks if it's a finite value. Therefore, it returns false on numbers that are NaN, Infinity or -Infinity.
    */ 
export const getDefaultNumber = (num) => {
if(Number.isFinite(num)){
    return num
}
return 0;
}