import { GET_CINEMA_MOVIES } from "services/constants";
import { produce } from "immer";
export const atCineaMoviesReducer = (
  state = {},
  action = { type: "", payload: "" }
) => {
  switch (action.type) {
    case GET_CINEMA_MOVIES:
      const data = action.payload.data.results;
      return produce(state, (draft) => {
        draft.data = data;
      })
    default:
      return state;
  }
};
