import { ACTION_KEY_GET_CURRENT_USER_COLLECTIONS, DUPLICATE_MOVIE_TITLE } from "services/constants";
import { produce } from "immer";


const initialState = {
  usersCollection: [],
  error: ""
};
export const saveCurrentUserCollections = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_KEY_GET_CURRENT_USER_COLLECTIONS:
      return produce(state, (draft) => {
        draft.usersCollection = action.payload
      })
      case DUPLICATE_MOVIE_TITLE:
        console.log(action.payload)
        return produce(state, (draft) => {
            draft.error  = action.payload
        })
    // return {
    //   ...state,
    //   usersCollection: action.payload
    // };
    default:
      return state;
  }
};
