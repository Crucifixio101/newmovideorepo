import { GET_LATEST_MOVIES } from "services/constants";
import RecentMoviesModel from "models/latest-movies-model";

export const LatestMoviesReducer = (state = {}, action = {}) => {
  switch (action.type) {
    case GET_LATEST_MOVIES:
      const data = action.payload.data.results;
      const payload = data.map(item => new RecentMoviesModel(item) )
      return {
        ...state,
        payload
      };
    default:
      return state;
  }
};
